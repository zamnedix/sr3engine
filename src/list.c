#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "error.h"
#include "log.h"

static int list_item_new(struct list_item** out) {
	if (!out) {
		return ENULLPTR;
	}
	*out = malloc(sizeof(struct list_item));
	if (!*out) {
		return EALLOC;
	}
	memset(*out, 0, sizeof(struct list_item));
	return 0;
}

int list_new(struct list** out) {
	if (!out) {
		return ENULLPTR;
	}
	*out = malloc(sizeof(struct list));
	if (!*out) {
		return EALLOC;
	}
	memset(*out, 0, sizeof(struct list));
	return 0;
}

int list_append(struct list* out, void* in, unsigned long inlen) {
	int ret;
	struct list_item* p;
	if (! out || ! in) {
		return ENULLPTR;
	}
	ret = list_item_new(&p);
	if (ret < 0) {
		return ret;
	}
	p->data = malloc(inlen);
	if (!p->data) {
		free(p);
		return EALLOC;
	}
	memcpy(p->data, in, inlen);
	p->datalen = inlen;
	out->tail->next = p;
	out->tail = p;
	return 0;
}

int list_del(struct list* out, void* data) {
	struct list_item* p;
	if (!out || !data) {
		return ENULLPTR;
	}
	p = out->head;
	while (p) {
		if (p->data == data) {
			if (p->prev) {
				p->prev->next = p->next;
			}
			if (p->next) {
				p->next->prev = p->prev;
			}
			if (out->head == p) {
				out->head = p->next;
			}
			if (out->tail == p) {
				out->tail = p->prev;
			}
			memset(p->data, 0, p->datalen);
			memset(p, 0, sizeof(struct list_item));
			free(p);
		}
	}
	return 0;
}

