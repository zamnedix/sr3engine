#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "log.h"

/*  log.c - 5-level logging interface
    Copyright (C) 2018  zamnedix

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
   @file
   @brief Logging utilities
*/

extern FILE* debugfp;
extern FILE* infofp;
extern FILE* warningfp;
extern FILE* errorfp;
extern FILE* fatalfp;

/**
 * Convert a log level to a human-readable string.
 * @param levelstr Destination output. Should be at least 8 bytes in size.
 * @param level Log level {DEBUG, INFO, WARNING, ERROR, FATAL}
 * @param len Length of levelstr buffer
 */
static void log_levelstr(char* levelstr, uint8_t level, size_t len) {
  switch(level) {
  case DEBUG: strncpy(levelstr, "debug", len); break;
  case INFO: strncpy(levelstr, "info", len); break;
  case WARNING: strncpy(levelstr, "warning", len); break;
  case ERROR: strncpy(levelstr, "error", len); break;
  case FATAL: strncpy(levelstr, "fatal", len); break;
  default: strncpy(levelstr, "log", len); break;
  }
}

/**
 * Write a message to a log
 * @param fp Log file to use {stderr, debug, ...}
 * @param level Log level {DEBUG, INFO, WARNING, ERROR, FATAL}
 * @param filename Source filename of invoking function
 * @param function Name of invoking function
 * @param fmt Log message with optional format specifiers
 * @param ... Optional variables for formats
 */
static void log_string(FILE* fp, uint8_t level, char* fmt, va_list ap) {
	char buf[4096];
	char levelstr[8];
	char timestr[32];
	time_t t = time(0);
	
	if (!fmt || !fp) {
		return;
	}
	memset(buf, 0, 4096);
	memset(levelstr, 0, 8);
	memset(timestr, 0, 32);

	if (!strftime(timestr, 32, "%c", localtime(&t))) {
		return;
	}
#if (C11_ENABLED || C99_ENABLED)
	vsnprintf(buf, 4096, fmt, ap);
#else
	vsprintf(buf, fmt, ap);
#endif
	log_levelstr(levelstr, level, 8);

	fprintf(fp, "[%s] [%s] : %s\n", timestr, levelstr, buf);
}
void log_debug(char* msg, ...) {
	va_list ap;
	if (!debugfp) {
		return;
	}
	va_start(ap, msg);
	log_string(debugfp, DEBUG, msg, ap);
	va_end(ap);
}
void log_info(char* msg, ...) {
	va_list ap;
	if (!infofp) {
		return;
	}
	va_start(ap, msg);
	log_string(infofp, INFO, msg, ap);
	va_end(ap);
}
void log_warning(char* msg, ...) {
	va_list ap;
	if (!warningfp) {
		return;
	}
	va_start(ap, msg);
	log_string(warningfp, WARNING, msg, ap);
	va_end(ap);
}
void log_error(char* msg, ...) {
	va_list ap;
	if (!errorfp) {
		return;
	}
	va_start(ap, msg);
	log_string(errorfp, ERROR, msg, ap);
	va_end(ap);
}
void log_fatal(char* msg, ...) {
	va_list ap;
	if (!fatalfp) {
		return;
	}
	va_start(ap, msg);
	log_string(fatalfp, FATAL, msg, ap);
	va_end(ap);
}
