#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include "isaac64.h"
#include "sr3engine.h"
#include "log.h"
#include "error.h"
#include "standalone.h"

const char* WELCOME =
	"\n\nIf playing with others, it is important that all players use\n"
	"the same seed for the same game session.\n"
	"On most Linux systems, you can generate one with the `od' command:\n"
	"\tod -An -tx1 -N8 /dev/urandom\n"
	"You can also visit this URL:\n"
	"\thttps://www.random.org/cgi-bin/randbyte?nbytes=8&format=h\n"
	"\nPlease enter a hex-encoded 8-byte seed:\n";

const char* HELP =
	"------ Session management ------\n"
	"load <path>   :: Import and resume a previously saved session.\n"
	"start <path>  :: Start a new session and log it to the given file.\n"
	"------ Actions ------\n"
	"record <comment>                   :: Record a text-only action.\n"
	"roll <dice> <sides> <tn> <comment> :: Roll dice. \n";

struct shell global = {0};

static int shell_help(struct shell* s)
{
	fprintf(s->outfp, HELP);
	return 0;
}

static int shell_load(struct shell* s)
{
	return ETODO;
}

static int shell_start(struct shell* s)
{
	struct arg* p;
	if (s->n_args < 2) {
		return EARGMIN;
	}
	p = &s->args[1];
	s->session = fopen(p->arg, "w+");
	if (!s->session) {
		return EOPEN;
	}
	fprintf(s->session, "%lx\n", s->seed);
	return SUCCESS;
}

static int shell_verify(struct shell* s)
{
	return ETODO;
}

static int shell_roll(struct shell* s)
{
	struct arg* p;
	unsigned dice;
	unsigned sides;
	unsigned tn;
	char* comment;
	unsigned commentlen;
	u8* result;
	int ret;
	if (s->n_args < 4) {
		return EARGMIN;
	}
	p = &s->args[1];
	dice = strtol(p->arg, NULL, 10);
	if (errno == ERANGE) {
		return EBUFOV;
	}
	p = &p[1];
	sides = strtol(p->arg, NULL, 10);
	if (errno == ERANGE) {
		return EBUFOV;
	}
	p = &p[1];
	tn = strtol(p->arg, NULL, 10);
	if (errno == ERANGE) {
		return EBUFOV;
	}
	p = &p[1];
	comment = p->arg;
	commentlen = p->arglen;
	if (!dice || !sides) {
		return 0;
	}
	result = malloc(sizeof(u8)*dice);
	if (!result) {
		return EALLOC;
	}
	memset(result, 0, sizeof(u8)*dice);
	ret = sr3_roll(result, dice, sides, tn, s->ctx);
	if (ret < 0) {
		return ret;
	}
	sr3_dump_roll(s->outfp, dice, sides, tn, result);
	if (s->session) {	    
		ret = sr3_log_roll(s->session, result, dice, sides, tn,
				   comment);
		if (ret) {
			return ret;
		}
	}
	return SUCCESS;
}

struct command commands[] = {
	{"help",   4, shell_help},
	{"load",   4, shell_load},
	{"start",  5, shell_start},
	{"verify", 6, shell_verify},
	{"roll",   4, shell_roll},
	{NULL, 0, NULL}
};


static int shell_dispatch(struct shell* s)
{
	struct arg* p;
	struct command* c;
	int ret;
	struct timespec tspre = {0};
	struct timespec tspost = {0};
	float exectime;
	if (!s) {
		return ENULLPTR;
	}
	if (!s->outfp || !s->infp) {
		return ENULLPTR;
	}
	/* Empty line, return immediately */
	if (!s->linelen || !s->n_args) {
		return 0;
	}
	p = &s->args[0];
	c = commands;
	while (c->name) {
		if (p->arglen == c->namelen) {
			if (!memcmp(p->arg, c->name, c->namelen)) {
				ret = timespec_get(&tspre, TIME_UTC);
				if (!ret) {
					return ECLOCK;
				}
				ret = c->func(s);
				if (ret) {
					return ret;
				}
				ret = timespec_get(&tspost, TIME_UTC);
				if (!ret) {
					return ECLOCK;
				}
				exectime = (tspost.tv_nsec - tspre.tv_nsec);
				exectime *= 0.000000001;
				exectime += (tspost.tv_sec - tspre.tv_sec);
				log_info("Execution completed in %fs",
					 exectime);
				break;
			}
		}
		c = &c[1];
	}
	return 0;
}

static int shell_step(struct shell* s)
{
	char input;
	struct arg* p;
	if (!s) {
		return ENULLPTR;
	}
	if (!s->outfp || !s->infp) {
		return ENULLPTR;
	}
	memset(s->args, 0, sizeof(struct arg) * 16);
	memset(s->line, 0, 256);
	s->n_args = 0;
	s->linelen = 0;
	fprintf(s->outfp, "sr3engine > ");
	fflush(s->outfp);
	while (s->linelen < 256) {
		input = fgetc(s->infp);
		if (input == '\n') {
			break;
		}
		if (input == ' ' || input == '\t') {
			s->n_args++;
		}
		else {
			if (s->n_args >= 16) {
				return EBUFOV;
			}
			p = &s->args[s->n_args];
			if (p->arglen < 16) {
				p->arg[p->arglen] = input;
				p->arglen++;
					  
			}
			else {
				return EBUFOV;
			}
		}
		s->line[s->linelen] = input;
		s->linelen++;		
	}
	if (s->linelen) {
		s->n_args++;
	}
	log_debug("got line with %u args", s->n_args);
	return shell_dispatch(s);
}

static int shell_run(struct shell* s)
{
	int ret;
	while (s->state) {
		ret = shell_step(s);
		if (ret < EWARNING) {
			log_error("Error interpreting command: %s",
				  sr3_strerror(ret));
		}
		else if (ret) {
			log_error("Error executing command: %s",
				  sr3_strerror(ret));
			return ret;
		}		
	}
	return 0;
}

int main()
{
	char buf[32] = {0};
	int len;
	uint64_t seed = 0;
	if (sr3_init()) {
		fprintf(stderr, "[FATAL]: failed to initialize engine\n");
		return 1;
	}
	log_debug("sr3engine - Initializing standalone client");
	fprintf(stdout, WELCOME);
	len = fread(buf, 1, 24, stdin);
	if (sr3_parse_seed(&seed, buf, len)) {
		log_fatal("failed to parse seed");
		return 1;
	}
	log_debug("Parsed seed: %llx", seed);
	log_debug("Initializing ISAAC64 CSPRNG");
	memset(&global, 0, sizeof(struct shell));
	if (isaac64_new(&(global.ctx))) {
		log_fatal("Allocation failure (Are you out of RAM?)");
		return 1;
	}
	isaac64_seed(global.ctx, &seed);
	global.state++;
	global.infp = stdin;
	global.outfp = stdout;
	global.seed = seed;
	return shell_run(&global);
}

	
