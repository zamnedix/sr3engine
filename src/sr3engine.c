#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "types.h"
#include "skill.h"
#include "log.h"
#include "isaac64.h"
#include "error.h"
#include "sr3engine.h"

FILE* debugfp;
FILE* fatalfp;
FILE* warningfp;
FILE* errorfp;
FILE* infofp;

int sr3_init() {
	debugfp = stderr;
	fatalfp = stderr;
	warningfp = stderr;
	errorfp = stderr;
	infofp = stderr;
	return 0;
}

int sr3_dump_roll(FILE* out, unsigned dice, unsigned sides, unsigned tn,
		   u8* in)
{
	unsigned i;
	unsigned pass = 0;
	char ts[128] = {0};
	time_t t = time(NULL);
	int ret;
	ret = strftime(ts, 127, "%c", localtime(&t));
	if (!ret) {
		return EBUFOV;
	}	
	fprintf(out, "[%s] Rolled %u %u-sided dice at TN %u: ",
		ts, dice, sides, tn);
	for (i = 0; i < dice; i++) {
		if (tn && in[i] >= tn) {
			if (fprintf(out, "\033[1m%u\033[0m ", in[i]) < 0) {
				return EFIO;
			}
			pass++;
		}
		else {
			if (fprintf(out, "%u ", in[i]) < 0) {
				return EFIO;
			}
		}
	}
	if (fprintf(out, "\n") < 0) {
		return EFIO;
	}
	if (fprintf(out, "%u successes\n", pass) < 0) {
		return EFIO;
	}
	if (fflush(out) < 0) {
		return EFIO;
	}
	return pass;
}

int sr3_roll(u8* out, unsigned dice, unsigned sides, unsigned tn,
	 struct isaac64* ctx)
{
	int test = 0;
	unsigned i;
	if (!out || !ctx) {
		return ENULLPTR;
	}
	if (sides > 255) {
		return EMAXSIDES;
	}
	for (i = 0; i < dice; i++) {
		out[i] = (isaac64_rand64(ctx) % (sides)) + 1;
		if (tn) {
			if (out[i] >= tn) {
				test++;
			}
		}
	}
	return test;
}

int sr3_log_roll(FILE* out, u8* in, unsigned dice, unsigned sides, unsigned tn,
	char* comment)
{
	unsigned i;
	if (fprintf(out, "%lu %u %u %u ", time(NULL), dice, sides, tn) < 0) {
		return EFIO;
	}
	for (i = 0; i < dice; i++) {
		if (fprintf(out, "%u ", in[i]) < 0) {
			return EFIO;
		}
	}
	if (fprintf(out, "%s ", comment) < 0) {
		return EFIO;
	}
	if (fputc('\n', out) < 0) {
		return EFIO;
	}
	fflush(out);
	return 0;
}

int sr3_parse_seed(uint64_t* out, char* input, unsigned inlen)
{
	unsigned inp;
	unsigned bufp;
	char buf[25] = {0};
	if (inlen > 24) {
		inlen = 24;
	}
	bufp = 0;
	inp = 0;
	while (inp < inlen) {
		if (input[inp] != ' ') {
			buf[bufp] = input[inp];
			buf[bufp+1] = input[inp+1];
			bufp+=2;
			inp+=2;
		}
		else {
			inp++;
		}
	}
	log_info("buf %s", buf);       
	*out = strtoull(buf, NULL, 16);
	if (errno == ERANGE) {
		return EBUFOV;
	}
	return 0;
}

static int sr3_logimport_step(struct sr3_importer* ctx)
{
	int ret;
	char* p = ctx->input;
	char* end;
	struct sr3_record* r;
	unsigned i;
	if (!ctx) {
		return ENULLPTR;
	}
	if (!ctx->input || !ctx->output) {
		return ENULLPTR;
	}
	r = malloc(sizeof(struct sr3_record));
	if (!r) {
		return EALLOC;
	}
	memset(r, 0, sizeof(struct sr3_record));
	r->date = strtol(p, &end, 10);
	p = end;
	r->dice = strtol(p, &end, 10);
	p = end;
	r->sides = strtol(p, &end, 10);
	p = end;
	r->tn = strtol(p, &end, 10);
	p = end;
	r->test = malloc(sizeof(u8) * r->dice);
	if (!r->test) {
		return EALLOC;
	}
	memset(r->test, 0, sizeof(u8) * r->dice);
	for (i = 0; i < r->dice; i++) {
		r->test[i] = strtol(p, &end, 10);
		p = end;
	}
	ctx->ip = p - ctx->input;
	end = memchr(ctx->input, '\n', ctx->inlen - ctx->ip);
	if (!end) {
		return EEOF;
	}	
	r->commentlen = (end - p);
	r->comment = malloc(r->commentlen + 1);
	if (!r->comment) {
		free(r->test);
		return EALLOC;
	}
	memcpy(r->comment, ctx->input, r->commentlen);
	r->comment[r->commentlen] = 0;
	ctx->ip = end - ctx->input;
	ret = list_append(ctx->output, r, sizeof(struct sr3_record));
	if (ret) {
		return ret;
	}
	return 0;
}

static int sr3_logimport_start(struct sr3_importer* ctx)
{
	int ret;
	if (!ctx) {
		return ENULLPTR;
	}
	while (ctx->ip < ctx->inlen) {
		ret = sr3_logimport_step(ctx);
		if (ret) {
			return ret;
		}
	}
}













