#include "error.h"

const char* sr3_strerror(int err) {
	switch(err) {
	case ENULLPTR:  return "Null pointer dereference";
	case EALLOC:    return "Allocation failure (Are you out of RAM?)";
	case EFP:       return "Floating-point error";
	case EMAXSIDES: return "A die may not have more than 255 sides";
	case EFIO:      return "File I/O error";
	case EBUFOV:    return "Buffer over/underflow";
	case EEOF:      return "Unexpected end-of-file";
		/* Warnings */
	case ETODO:     return "Not yet implemented";
	case EARGMIN:   return "Not enough arguments (see \"help\")";
	case EOPEN:     return "File open failed";
	case ECLOCK:    return "Failed to retrieve system clock time";
	}
	return "Unrecognized error code";
}
