#include <fenv.h>
#include <stdio.h>
#include "types.h"
#include "character.h"
#include "isaac64.h"
#include "error.h"

static int char_update_reaction(struct character* c)
{
	float x;
	int mode;
	if (!c) {
		return ENULLPTR;
	}
	mode = fegetround();
	if (mode < 0) {
		return EFP;
	}
	if (fesetround(FE_DOWNWARD) < 0) {
		return EFP;
	}
	x = (c->QUI + c->INT) / 2.0;
	if (fesetround(mode) < 0) {
		return EFP;
	}
	c->REA = (int)x;
	return 0;
}

static int char_update_cpool(struct character* c)
{
	float x;
	int mode;
	if (!c) {
		return ENULLPTR;
	}
	mode = fegetround();
	if (mode < 0) {
		return EFP;
	}
	if (fesetround(FE_DOWNWARD) < 0) {
		return EFP;
	}
	x = (c->QUI + c->INT + c->WIL) / 2.0;
	if (fesetround(mode) < 0) {
		return EFP;
	}
	c->cpool = (int)x;
	return 0;
}

static int char_update_acpool(struct character* c)
{
	float x;
	int mode;
	if (!c) {
		return ENULLPTR;
	}
	mode = fegetround();
	if (mode < 0) {
		return EFP;
	}
	if (fesetround(FE_DOWNWARD) < 0) {
		return EFP;
	}
	x = (c->CHA + c->INT + c->WIL) / 2.0;
	if (fesetround(mode) < 0) {
		return EFP;
	}
	c->cpool = (int)x;
	return 0;
}

int char_set_bod(struct character* c, u8 val)
{
	if (!c) {
		return ENULLPTR;
	}
	c->BOD = val;
	return 0;
}

int char_set_str(struct character* c, u8 val)
{
	if (!c) {
		return ENULLPTR;
	}
	c->STR = val;
	return 0;
}

int char_set_qui(struct character* c, u8 val)
{
	int ret = 0;
	if (!c) {
		return ENULLPTR;
	}
	c->QUI = val;
	ret += char_update_reaction(c);
	ret += char_update_cpool(c);
	return ret;
}

int char_set_int(struct character* c, u8 val)
{
	int ret = 0;
	if (!c) {
		return ENULLPTR;
	}
	c->INT = val;
	ret += char_update_reaction(c);
	ret += char_update_cpool(c);
	ret += char_update_acpool(c);
	return ret;
}	

int char_set_wil(struct character* c, u8 val)
{
	int ret = 0;
	if (!c) {
		return ENULLPTR;
	}
	c->WIL = val;
	ret += char_update_cpool(c);
	ret += char_update_acpool(c);
	return 0;
}

int char_set_cha(struct character* c, u8 val)
{
	int ret = 0;
	if (!c) {
		return ENULLPTR;
	}
	c->CHA = val;
	ret += char_update_acpool(c);
	return 0;
}

int char_set_rea(struct character* c, u8 val)
{
	if (!c) {
		return ENULLPTR;
	}
	c->REA = val;
	return 0;
}

int char_set_mag(struct character* c, u8 val)
{
	if (!c) {
		return ENULLPTR;
	}
	c->MAG = val;
	return 0;
}

int char_set_ess(struct character* c, u8 val)
{
	if (!c) {
		return ENULLPTR;
	}
	c->ESS = val;
	return 0;
}


