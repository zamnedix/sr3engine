/* Copyright 2020 Joshua Mallad <zamnedix@zamnedix.net> */
/* This file is part of libisaac64, an implementation of Bob Jenkins'          
   pseudo-random number generator. ISAAC64 is described at the following URL:  
   https://burtleburtle.net/bob/rand/isaacafa.html
   This file may be used under the terms of EITHER the MIT License
   (see mit_license.txt) OR the GNU General Public License v3.0 
   (see gpl-3.0.txt). */

#pragma once
#include <stdint.h>

/* This opaque struct represents one ISAAC64 context. */
struct isaac64;

/* Zero and free an ISAAC64 context. */
void isaac64_free(struct isaac64* ctx);
/* Allocate and zero a new ISAAC64 context. 
   Returns 0 for success or a negative error code. */
int isaac64_new(struct isaac64** ctx);
/* Zero an ISAAC64 context and initialize it from a supplied 2048-byte buffer  
   of entropy. 
   Returns 0 for success or a negative error code. */
int isaac64_init(struct isaac64* ctx, void* in);
/* Zero an ISAAC64 context and initialize it from an 8-byte seed using         
   the smaller SplitMix64 PRNG. 
   Returns 0 for success or a negative error code. */
int isaac64_seed(struct isaac64* ctx, void* in);
/* Fill the `out' buffer with `sz' bytes of entropy from supplied context.
   Returns 0 for success or a negative error code. */
int isaac64_fill(struct isaac64* ctx, void* out, long sz);
/* Retrieve the next 64 bits from the given context. */
uint64_t isaac64_rand64(struct isaac64* ctx);
/* Return a human-readable explanation of an error code.
   This function always succeeds. */	
const char* isaac64_strerror(int err);

