#pragma once

#include <stdio.h>

struct arg {
	char arg[16];
	unsigned arglen;
};

/*struct var {
	char name[16];
	
	};*/

struct shell {
	char line[256];
	struct arg args[16];
	unsigned linelen;
	unsigned n_args;
	FILE* outfp;
	FILE* infp;
	/* sr3engine variables */
	struct isaac64* ctx;
	FILE* session;
	uint64_t seed;
	u8 state;
};

struct command {
	char* name;
	unsigned namelen;
	int (*func)(struct shell*);
};
