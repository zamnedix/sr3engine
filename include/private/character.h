#pragma once
#include "types.h"
#include "skill.h"
#include "list.h"

struct character {
	u8 BOD;
	u8 STR;
	u8 QUI;
	u8 INT;
	u8 CHA;
	u8 WIL;
	u8 REA;
	u8 MAG;
	u8 ESS;
	unsigned cpool;
	unsigned acpool;
	struct list* skills;
	unsigned n_skills;
};
