#pragma once

/**
   @file
*/

#include <stdio.h>
#include <stdint.h>

#define DEBUG 0
#define INFO 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

void log_debug(char* msg, ...);
void log_info(char* msg, ...);
void log_warning(char* msg, ...);
void log_error(char* msg, ...);
void log_fatal(char* msg, ...);



