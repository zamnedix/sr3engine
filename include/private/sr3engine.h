#pragma once

#include <time.h>
#include "list.h"
#include "types.h"

struct sr3_record {
	time_t date;
	u8* test;
	unsigned dice;
	unsigned sides;
	unsigned tn;
	char* comment;
	unsigned commentlen;
};

struct sr3_importer {
	/* Inputs */
	uint64_t seed; /* Seed for the PRNG. */
	char* input;      /* Input buffer containing log data */
	unsigned long inlen; /* Length of the input buffer */
	unsigned long ip; /* Location in the input buffer */
	/* Outputs */
	struct list* output;
	size_t n_records;
};

int sr3_init();
int sr3_dump_roll(FILE* out, unsigned dice, unsigned sides, unsigned tn,
		   u8* in);
int sr3_roll(u8* out, unsigned dice, unsigned sides, unsigned tn,
	     struct isaac64* ctx);
int sr3_log_roll(FILE* out, u8* in, unsigned dice, unsigned sides, unsigned tn,
		 char* comment);
int sr3_parse_seed(uint64_t* out, char* input, unsigned inlen);

	
