#pragma once

#include <stddef.h>

struct list_item;
struct list_item {
	void* data;
	unsigned long datalen;
	struct list_item* prev;
	struct list_item* next;
};
struct list {
	struct list_item* head;
	struct list_item* tail;
	unsigned long n_items;
};

int list_new(struct list** out);
int list_append(struct list* out, void* in, unsigned long inlen);
int list_del(struct list* out, void* data);
